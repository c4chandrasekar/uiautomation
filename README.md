Etergo
===========

WebdriverI/O

# Installation

```shell

npm i

npm install webdriverio
```

or if you want to use the wdio test runner

```shell
npm install -g webdriverio
```

You can also install the package globally on your machine and use the `wdio` directly from the command line. However 
it is recommended to install it per project.

## Set up your Selenium environment

There are two ways of setting up your Selenium environment: as a standalone package or by installing the
server and browser driver separately.

### Use of existing standalone package

The simplest way to get started is to use one of the NPM selenium standalone
packages like: [vvo/selenium-standalone](https://github.com/vvo/selenium-standalone). After installing
it (globally) you can run your server by executing:

```sh
npm install selenium-standalone --save-dev

./node_modules/.bin/selenium-standalone install && ./node_modules/.bin/selenium-standalone start
```

If you are using the wdio testrunner you might be interested in the [Selenium Standalone Service]
(/guide/services/selenium-standalone.html) that starts the server for you everytime before the test starts.

You can also run the selenium server separately from your test setup. To do so you need to get the newest version of 
the selenium standalone server [here](http://docs.seleniumhq.org/download/). Just download the jar file somewhere on 
your system. After that start your terminal and execute the file via

```sh
$ java -jar /your/download/directory/selenium-server-standalone-3.5.3.jar
```

With the latest version of Selenium most of the drivers for the browser come with an external driver that has to be 
downloaded and setup.

##Setup Appium

brew install node         # get node.js

npm install -g appium     # get appium

npm install wd            # get appium client

appium &                  # start appium

node your-appium-test.js

### Verifying the Installation

To verify that all of Appium's dependencies are met you can use
`appium-doctor`.  Install it with `npm install -g appium-doctor`, then run the
`appium-doctor` command, supplying the `--ios` or `--android` flags to verify
that all of the dependencies are set up correctly.

### Starting Appium

Now we can kick up an Appium server, either by running it from the command line
like so (assuming the NPM install was successful):

```
appium
```
Or by clicking the huge Start Server button inside of Appium Desktop.

Appium will now show you a little welcome message showing the version of Appium
you're running and what port it's listening on (the default is `4723`). This
port information is vital since you will have to direct your test client to
make sure to connect to Appium on this port. If you want to change, the port,
you can do so by using the `-p` flag when starting Appium (be sure to check out
the full list of [server
parameters](/docs/en/writing-running-appium/server-args.md)).

### After setup 

Please generate a build and give the path in wdio.android.conf.js -> capabilities -> app
 and also mention the device platformName and platformVersion.

### Run

In terminal run the below command
```sh
$ wdio wdio.android.conf.js
```

