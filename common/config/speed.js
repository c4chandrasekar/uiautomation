var argv       = require( 'yargs' ).argv;
var colors     = require( 'colors' );
var multiplier = argv.speed || 3;

console.info( colors.magenta( `browser.pause speed multiplier is: ${multiplier}x` ) );
module.exports = {
  fast      : 250 * multiplier,
  faster    : 500 * multiplier,
  normal    : 1000 * multiplier,
  slower    : 2000 * multiplier,
  slow      : 5000 * multiplier,
  snail     : 1000 * multiplier,
  implicit  : 1000 * multiplier,
  maxSpec   : Math.max( 15000, 15000 * multiplier ),
  multiplier: multiplier
};