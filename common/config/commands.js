/**
 * Created by gtontche on 10/6/16.
 */
var argv   = require( 'yargs' ).argv;
var colors = require( 'colors' );
var _      = require( 'lodash' );
var speed  = require( './speed' );

var config = {
  speed: argv.speed || 1
};

config.getCustomCommands = function() {

  browser.waitForAlert = function() {
    browser.pause( 2500 );
    browser.alertAccept();
  };

  browser.logExisting = function( element, scope ) {
    var selector   = _.isString( element ) ? element : browser.getSelector( element, scope );
    var isExisting = browser.isExisting( selector );

    console.log( colors.green( selector ), isExisting ? colors.green( 'exists' ) : colors.red( 'not exist' ) );
  };

  browser.getElement = function( selector, scope ) {
    selector = _.isNil( scope ) ? selector : browser.getSelector( selector, scope );
    var elem = browser.element( selector );

    return elem;
  };

  /**
   * Used for interpolating selectors with placeholders with data
   *
   * from:http://stackoverflow.com/questions/1408289/how-can-i-do-string-interpolation-in-javascript
   * usage:
   *  getSelector('[qd-tag="I am ${NPI}"]', {'npi': 12344555})
   *  => "I am 5
   *  getSelector('[qd-tag="I am ${0}"]', [123456677])
   *  => "I am 5
   *
   * @param template
   * @param scope
   * @returns {string}
   */
  browser.getSelector = function( template = '', scope = {} ) {
    return template.replace( /\${([^{}]*)}/g,
      function( match, prop ) { //e.g. match == "${NPI}", prop == "NPI"
        var replacement = _.get( scope, prop, '' );
        return typeof replacement === 'string' || typeof replacement === 'number' ? replacement : match;
      }
    );
  };

  /*================================================================================
   =                                  wait and DO stuff                            =
   ================================================================================*/

  browser.waitAndSetValue = function( selector, value, maxWait = 5000, trailingWait = 250 ) {

    browser.waitAndAction( selector, setValue, maxWait, trailingWait );

    function setValue( element ) {
      logSelector( selector, 'set', value );
      element.setValue( value );
    }

  };

  browser.waitAndClick = function( selector, maxWait = 1000, trailingWait = 250 ) {

    browser.waitAndAction( selector, click, maxWait, trailingWait );

    function click( element ) {
      logSelector( selector, 'click' );
      element.click();
    }

  };

  browser.waitAndSelect = function( selector, value, maxWait = 5000, trailingWait = 250 ) {

    browser.waitAndAction( selector, selectVal, maxWait, trailingWait );

    function selectVal( element ) {
      logSelector( selector, 'select', value );
      element.selectByValue( value );
    }

  };

  /**
   * reusable wait and do stuff method - waits for an element to exist and then act on it
   * @param selector
   * @param actionCb      - method to call once the element exists
   * @param maxWait       - max wait time in ms
   * @param trailingWait  - ms to pause once actions has been performed
   * @returns element     - for chaining
   */
  browser.waitAndAction = function( selector, actionCb, maxWait = 5000, trailingWait = 250 ) {

    logSelector( selector, 'wait' );
    browser.waitForVisible( selector, maxWait );

    var element = browser.element( selector );
    actionCb( element );

    browser.pause( trailingWait );
    return element;
  };


  /*================================================================================
   =                           wait and CHECK stuff                                =
   ================================================================================*/
  /**
   * wait a condition to meet where the condition is defined in the callback
   *
   * @description if the condition hasn't been meet, check every 250ms until condition has been met or maxWait has been reached
   *
   * @param conditionCb - return true if condition has been met
   */
  
  browser.waitForCondition = function( conditionCb, maxWait = 5000, trailingWait = 250 ) {
    var frequency = 250;
    var isValid   = false;
    browser.pause( frequency ); // time before condition met
    while ( maxWait > 0 && !isValid ) {
      isValid = conditionCb();
      browser.pause( frequency );
      maxWait = maxWait - frequency;
    }
    browser.pause( trailingWait ); //post spinner processing takes time
  };

  /**
   * wait for spinner to disappear (display:none)
   * @param maxWait
   */
  browser.waitForSpinner = function( selector = '[class="spinner-box"]', maxWait = 5000, trailingWait = 250 ) {

    browser.waitForInvisible( selector, maxWait, trailingWait );
    return browser;

  };

  browser.waitForInvisible = function( selector, maxWait = 5000, trailingWait = 250 ) {
    browser.waitForCondition( () => {
      return !browser.isVisible( selector, maxWait, trailingWait );
    } );
    return browser;
  };

  browser.waitForUrlChange = function( fromUrl = browser.getUrl() ) {
    browser.waitForCondition( () => {
      return fromUrl != browser.getUrl();
    } );
    browser.waitForSpinner();
    return browser;
  };


  /*=========================================================================
   =                       OVERRIDE browser.pause                           =
   ==========================================================================*/
  var origPause = browser.pause;
  browser.pause = function( timer ) {
    origPause.call( browser, timer * speed.multiplier );
  };

  function logSelector( selector, prefix, value ) {
    switch ( prefix ) {
      case "wait":
        console.log( '  wait:  ', colors.gray( selector ) );
        break;
      case "click":
        console.log( colors.bold( '- Click: ' ), colors.green( selector ) );
        break;
      case "set":
        console.log( colors.bold( '- Set:   ' ), colors.green( selector ), "=>", colors.green.bold( value ) );
        break;
      case "select":
        console.log( colors.bold( '- Select:' ), colors.green( selector ), "=>", colors.green.bold( value ) );
        break;
      default:
        console.log( selector );
    }
  }
};

module.exports = config;