/**
 * Created by hzou on 2/5/2016.
 */

var customCommands = require('./commands');
var speed = require('./speed');
var config = {

    host: 'localhost',
    port: 4444,
    specs: [
        './specs/*.js'
    ],
    capabilities: [{
        browserName: 'chrome'
    }],
    maxInstances: 2,
    debug: true,
    sync: true,
    logLevel: 'error',
    bail: 1,
    coloredLogs: true,
    screenshotPath: './errorShots/',
    baseUrl: 'localhost:3000',
    waitforTimeout: 5000,
    connectionRetryTimeout: 10000,
    connectionRetryCount: 1,
    framework: 'jasmine',
    reporters: ['spec'],
    reporterOptions: {
        outputDir: './logs',
        writeStandardOutput: true
    },

    jasmineNodeOpts: {
        defaultTimeoutInterval: speed.maxSpec,  //max time for each test
        expectationResultHandler: function (passed, assertion) {
            // do something
        }
    },
    before: function (capabilities, specs) {
        //shouldn't take more than 1 sec to find selector
        browser.timeouts('implicit', speed.implicit);
        customCommands.getCustomCommands();
    }

};


exports.config = config;