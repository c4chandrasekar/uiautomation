"use strict";
var colors = require( 'colors' );
var _      = require( 'lodash' );
var speed  = require( '../config/speed' );
var request= require("request");
var Crypto = require("crypto");

class Page {
  constructor() {
    this.selectors = {};
    this.elements  = {};
    this.model     = {};
    this.makeProperties( this.selectors, this.rawSelectors, browser.getSelector );
    this.makeProperties( this.elements, this.rawSelectors, browser.getElement );
    // this.selectors = this.rawSelectors;
    // this.makeElements( this.selectors, this.rawSelectors );

    //adding different components to the page
    _.forEach( this.components, ( component ) => {
      if ( component.name ) {
        this[component.name.toLowerCase()] = component;
      }
      else {
        _.extend( this, component );
      }
    } );
  }

  makeProperties( ctx, rawSelectors, fn ) {
    _.forEach( rawSelectors, ( selector, name ) => {
      if ( _.isPlainObject( selector ) ) {
        ctx[name] = {};
        this.makeProperties( ctx[name], selector,fn ); //recursively create elements
      } else {
        Object.defineProperty( ctx, name, {
          get       : () => fn( selector, this.model ),
          enumerable: true
        } );
      }
    } );
  }

  get speed() {
    return speed;
  }

  /**
   * meant to be overridden by inherited classes
   * to include another "page". e.g. banner and footer
   *
   * @returns {Array}
   */
  get components() {
    return [];
  }

  get rawBindings() { //used to generate 1 day data-binding. e.g. update DOM when model value changes
    throw "not implemented";
  }

  /**
   * Specify the selectors here for the base class to autogenerate the elements from
   * @returns {{}}
   */
  get rawSelectors() {  //used for generate selectors and elements
    console.warn( 'no selector defined' );
    return {};
  }

  getElement( selector ) {
    return browser.getElement( selector, this );
  }

  getSelector( template = '' ) {
    return browser.getSelector( template, this );
  }
  
  selectRandom( selector, values)
  {
    var optionsCount = Object.keys(values).length;
    var ix = Math.floor(Math.random()*optionsCount);
    var selectValue = _.values(values)[ix];
    browser.waitAndSelect(selector,selectValue);
  }
}

module.exports = Page;
