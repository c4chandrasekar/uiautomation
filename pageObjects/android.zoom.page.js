var colors = require('colors');
var BasePage = require('../common/pageObjects/base.page.js');

"use strict";

class pageZoom {

    get selectors() {  //used for generate selectors and elements
        return {
            header: '//*[@text="This is a technical assessment"]',
            zoomIn: '//*[@resource-id="com.etergo.techassessment:id/btnZoomIn"]',
            zoomOut: '//*[@resource-id="com.etergo.techassessment:id/btnZoomOut"]',
            text100: '//*[@text="100%"]',
            text25: '//*[@text="25%"]',
            text75: '//*[@text="75%"]',
            text50: '//*[@text="50%"]',
            text125: '//*[@text="125%"]',
            text150: '//*[@text="150%"]',
            text175: '//*[@text="175%"]',
            text200: '//*[@text="200%"]'

        };
    }

    checkHeader(){
        browser.waitForVisible(this.selectors.header);
    }

    checkZoomButtonsVisible(){
        browser.waitForVisible(this.selectors.zoomIn);
        browser.waitForVisible(this.selectors.zoomOut);
    }

    checkZoomButtonEnabled(){
        browser.waitForEnabled(this.selectors.zoomIn);
        browser.waitForEnabled(this.selectors.zoomOut);
    }

    pageZoomPercentage(){
        browser.waitForVisible(this.selectors.text100);
    }

    increaseZoomLevel(){
        browser.waitAndClick(this.selectors.zoomIn);
        browser.waitForVisible(this.selectors.text125);
        browser.waitAndClick(this.selectors.zoomIn);
        browser.waitForVisible(this.selectors.text150);
        browser.waitAndClick(this.selectors.zoomIn);
        browser.waitForVisible(this.selectors.text175);
        browser.waitAndClick(this.selectors.zoomIn);
        browser.waitForVisible(this.selectors.text200);
    }

    lowestZoomPercentage(){
        browser.waitForVisible(this.selectors.text25);
    }

    validateZoomIn(){
        browser.waitForEnabled(this.selectors.zoomIn);
        browser.waitForEnabled(this.selectors.zoomOut);
        browser.waitAndClick(this.selectors.zoomIn);
    }

    maxZoomLevel(){
        browser.waitAndClick(this.selectors.zoomIn);
        browser.waitForVisible(this.selectors.text50);
        browser.waitAndClick(this.selectors.zoomIn);
        browser.waitForVisible(this.selectors.text75);
        browser.waitAndClick(this.selectors.zoomIn);
        browser.waitForVisible(this.selectors.text100);
        browser.waitAndClick(this.selectors.zoomIn);
        browser.waitForVisible(this.selectors.text125);
        browser.waitAndClick(this.selectors.zoomIn);
        browser.waitForVisible(this.selectors.text150);
        browser.waitAndClick(this.selectors.zoomIn);
        browser.waitForVisible(this.selectors.text175);
        browser.waitAndClick(this.selectors.zoomIn);
        browser.waitForVisible(this.selectors.text200);
    }

    checkLowestZoom(){
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitForVisible(this.selectors.text175);
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitForVisible(this.selectors.text150);
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitForVisible(this.selectors.text125);
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitForVisible(this.selectors.text100);
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitForVisible(this.selectors.text75);
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitForVisible(this.selectors.text50);
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitForVisible(this.selectors.text25);
    }

    ZoomOutFully(){
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitAndClick(this.selectors.zoomOut);
        browser.waitAndClick(this.selectors.zoomOut);
    }

    validateZoomOut(){
        browser.waitForEnabled(this.selectors.zoomOut);
        browser.waitForEnabled(this.selectors.zoomIn);
        browser.waitAndClick(this.selectors.zoomOut);

    }

}

module.exports = new pageZoom();
