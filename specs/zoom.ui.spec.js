var pageZoom = require('../pageObjects/android.zoom.page');
var configPage = require('../wdio.android.conf.js');

describe("spec description: ", function () {


    beforeAll(function () {
        console.log('>>> Start  ');
    });


    afterAll(function() {
        console.log('>>> END  ');
    });

    // Initial check for the app to see zoom buttons are enabled and 100% is displayed

    it('User launches app and validates header, buttons and zoom percentage', function () {
        pageZoom.checkHeader();
        pageZoom.checkZoomButtonsVisible();
        pageZoom.checkZoomButtonEnabled();
        pageZoom.pageZoomPercentage();

    });

    // Test case to check all zoom-in percentages

    it('User increases the zoom gradually and checks the zoom percentage',function () {
        pageZoom.increaseZoomLevel();
        if(browser.isEnabled(pageZoom.selectors.zoomIn)){
            console.log("Zoom in enabled");
            browser.close();
        }
        else{
            console.log("Zoom-in disabled");
        }

    });

    // Test case to check the maximum zoom percentage

    it('user increases the zoom from lowest to highest and checks the zoom percentage',function () {
        pageZoom.ZoomOutFully();
        pageZoom.lowestZoomPercentage();
        pageZoom.maxZoomLevel();
        if(browser.isEnabled(pageZoom.selectors.zoomIn)){
            console.log("Zoom-in enabled");
            browser.close();
        }
        else{
            console.log("Zoom-in disabled");
        }

    });

    // Test case to check when Zoom-in is enabled and disabled

    it('User checks for disable and enable zoom-in', function () {
        var temp = browser.isEnabled(pageZoom.selectors.zoomIn);
        if(temp == false){
            browser.waitAndClick(pageZoom.selectors.zoomOut);
        }
        pageZoom.validateZoomIn();

    });

    // Test case to check all zoom-out percentages

    it('User zooms out gradually and check the percentage of zoom', function () {
        pageZoom.checkLowestZoom();
        if(browser.isEnabled(pageZoom.selectors.zoomOut)){
            console.log("Zoom-out enabled");
            browser.close();
        }
        else{
            console.log("Zoom-out disabled");
        }

    });

    // Test case to check the minimum zoom-out percentage

    it('User zooms out completely and checks the percentage of zoom', function () {
        pageZoom.maxZoomLevel();
        pageZoom.ZoomOutFully();
        if(browser.isEnabled(pageZoom.selectors.zoomOut)){
            console.log("Zoom-out enabled");
            browser.close();
        }
        else{
            console.log("Zoom-out disabled");
        }
    });

    // Test case to check when Zoom-out is enabled and disabled

    it('User checks for disable and enable zoom-out', function () {
        var temp = browser.isEnabled(pageZoom.selectors.zoomOut);
        if( temp == false){
            browser.waitAndClick(pageZoom.selectors.zoomIn);
        }
        pageZoom.validateZoomOut();

    });

});
